package com.archison.letshows.presentation.matchers;

import android.support.test.espresso.matcher.BoundedMatcher;
import android.view.View;
import android.widget.RatingBar;

import org.hamcrest.Description;

/**
 *
 * Created by Luis on 24/2/18.
 */
public class RatingBarMatcher {

    public static BoundedMatcher<View, RatingBar> ratingBarWithRating(final float rating) {
        return new BoundedMatcher<View, RatingBar>(RatingBar.class) {

            @Override
            protected boolean matchesSafely(RatingBar ratingBar) {
                return ratingBar.getRating() == rating;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Checking the matcher on view: ");
                description.appendText("with expectedValue = " + rating);
            }

        };
    }
}
