package com.archison.letshows.presentation.tvshowslist;

import android.support.test.espresso.intent.rule.IntentsTestRule;

import com.archison.letshows.R;
import com.archison.letshows.presentation.mock.MockModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.archison.letshows.presentation.matchers.RatingBarMatcher.ratingBarWithRating;
import static org.mockito.Mockito.when;

/**
 * TODO: Presenter not being properly mocked, I'd have to use DI to properly avoid network calls, for example.
 *
 * Created by Luis on 23/2/18.
 */
public class TvShowsListActivityTest {

    private IntentsTestRule<TvShowsListActivity> mActivityRule =
            new IntentsTestRule<>(TvShowsListActivity.class);

    @Mock
    private TvShowsListPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mActivityRule.launchActivity(null);
        mActivityRule.getActivity().setPresenter(mPresenter);
        when(mPresenter.getTvShowList()).thenReturn(MockModel.getMockTvShowList());
    }

    @Test
    public void should_have_proper_basic_tv_show_model_content() {
        // TODO: When implemented DI, it'd be more easy to avoid the presenter to actually call the service...
        onView(withId(R.id.tvshow_item_textview))
                .check(matches(withText(MockModel.TITLE)));

        onView(withId(R.id.tvshow_vote_average_textview))
                .check(matches(withText(
                        mPresenter.getTvShowList().get(0).getFormattedVoteAverage())));

        onView(withId(R.id.tvshow_ratingbar))
                .check(matches(ratingBarWithRating(
                        mPresenter.getTvShowList().get(0).getAverageRatingForFiveStars())));
    }

}