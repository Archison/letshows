package com.archison.letshows.presentation.tvshowdetail;

import android.content.Intent;
import android.support.test.espresso.intent.rule.IntentsTestRule;

import com.archison.letshows.R;
import com.archison.letshows.presentation.mock.MockModel;
import com.archison.letshows.presentation.model.TvShowModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.archison.letshows.presentation.matchers.RatingBarMatcher.ratingBarWithRating;
import static org.mockito.Mockito.when;

/**
 * TODO: Presenter not being properly mocked, I'd have to use DI to properly avoid network calls, for example.
 *
 * Created by Luis on 21/2/18.
 */
@RunWith(JUnit4.class)
public class TvShowDetailActivityTest {

    private IntentsTestRule<TvShowDetailActivity> mActivityRule =
            new IntentsTestRule<>(TvShowDetailActivity.class);

    private TvShowModel mTvShowModel;

    @Mock
    private TvShowDetailPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mTvShowModel = MockModel.getMockTvShowModel();

        Intent intent = new Intent();
        intent.putExtra(TvShowDetailActivity.EXTRA_TV_SHOW_MODEL, mTvShowModel);
        mActivityRule.launchActivity(intent);

        mActivityRule.getActivity().setPresenter(mPresenter);
        when(mPresenter.getTvShowDetailModel()).thenReturn(MockModel.getMockTvShowDetailModel());
        when(mPresenter.getSimilarTvShowModelList()).thenReturn(new ArrayList<TvShowModel>());
    }

    @Test
    public void should_have_proper_basic_tv_show_model_content() {
        onView(withId(R.id.tvshow_title_textview))
                .check(matches(withText(MockModel.TITLE)));

        onView(withId(R.id.tvshow_vote_average_textview))
                .check(matches(withText(mTvShowModel.getFormattedVoteAverage())));

        onView(withId(R.id.tvshow_ratingbar))
                .check(matches(ratingBarWithRating(mTvShowModel.getAverageRatingForFiveStars())));

        onView(withId(R.id.tvshow_overview_textview))
                .check(matches(withText(mTvShowModel.getOverview())));

    }
}