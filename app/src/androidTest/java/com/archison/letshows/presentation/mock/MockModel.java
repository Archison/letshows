package com.archison.letshows.presentation.mock;

import com.archison.letshows.presentation.model.TvShowDetailModel;
import com.archison.letshows.presentation.model.TvShowGenreModel;
import com.archison.letshows.presentation.model.TvShowModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * Created by Luis on 24/2/18.
 */
public class MockModel {

    public static final String TITLE = "Interstellar";

    public static TvShowModel getMockTvShowModel() {
        return new TvShowModel(
                1,
                "nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg",
                TITLE,
                "An amazing movie.",
                10.0f,
                100000
        );
    }

    public static TvShowDetailModel getMockTvShowDetailModel(){
        return new TvShowDetailModel(
                1,
                "http://image.tmdb.org/t/p/w720/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg",
                TITLE,
                "An amazing movie.",
                10.0f,
                100000,
                getGenreList(),
                1,
                1,
                "2014-10-26",
                "Ended",
                100

        );
    }

    private static List<TvShowGenreModel> getGenreList() {
        List<TvShowGenreModel> genresList = new ArrayList<>();
        genresList.add(new TvShowGenreModel(1, "Awesome"));
        genresList.add(new TvShowGenreModel(2, "Sci-Fi"));
        return genresList;
    }

    public static List<TvShowModel> getMockTvShowList() {
        return Arrays.asList(getMockTvShowModel(), getMockTvShowModel());
    }
}
