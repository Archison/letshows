package com.archison.letshows.data.service.entity

/**
 *
 * Created by Luis on 17/2/18.
 */
class TvShowDetailEntitiy {

    var id: Int? = 0
    var poster_path: String? = null
    var vote_average: Float = 0.toFloat()
    var vote_count: Int = 0
    var name: String? = null
    var overview: String? = null
    var genres: List<TvShowGenreEntitiy>? = null
    var first_air_date: String? = null
    var number_of_episodes: Int = 0
    var number_of_seasons: Int = 0
    var status: String? = null
    var popularity: Float = 0.toFloat()

}