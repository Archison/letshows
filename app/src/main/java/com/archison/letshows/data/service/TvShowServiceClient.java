package com.archison.letshows.data.service;

import com.archison.letshows.BuildConfig;
import com.archison.letshows.data.service.entity.PopularTvShowsEntity;
import com.archison.letshows.data.service.entity.TvShowDetailEntitiy;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * Created by Luis on 16/2/18.
 */
public class TvShowServiceClient {

    private TvShowsService service;

    public TvShowServiceClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.TMDB_ENDPOINT)
                .build();

        service = retrofit.create(TvShowsService.class);
    }

    public Observable<PopularTvShowsEntity> getPopularTvShows(int page) {
        return service.getPopularTvShows(BuildConfig.API_KEY, page);
    }

    public Observable<PopularTvShowsEntity> getSimilarTvShows(int tvShowId, int page) {
        return service.getSimilarTvShows(tvShowId, BuildConfig.API_KEY, page);
    }

    public Observable<TvShowDetailEntitiy> getTvShowDetail(int tvShowId) {
        return service.getTvShowDetail(tvShowId, BuildConfig.API_KEY);
    }

}
