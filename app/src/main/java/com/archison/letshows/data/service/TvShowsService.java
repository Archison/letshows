package com.archison.letshows.data.service;

import com.archison.letshows.data.service.entity.PopularTvShowsEntity;
import com.archison.letshows.data.service.entity.TvShowDetailEntitiy;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 *
 * Created by Luis on 16/2/18.
 */
public interface TvShowsService {

    @GET("/3/tv/popular")
    Observable<PopularTvShowsEntity> getPopularTvShows(@Query("api_key") String apiKey, @Query("page") Integer page);

    @GET("/3/tv/{tv_id}/similar")
    Observable<PopularTvShowsEntity> getSimilarTvShows(@Path("tv_id") Integer tvShowId, @Query("api_key") String apiKey, @Query("page") Integer page);

    @GET("/3/tv/{tv_id}")
    Observable<TvShowDetailEntitiy> getTvShowDetail(@Path("tv_id") Integer tvShowId, @Query("api_key") String apiKey);

}
