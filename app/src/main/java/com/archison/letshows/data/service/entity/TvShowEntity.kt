package com.archison.letshows.data.service.entity

/**
 *
 * Created by Luis on 17/2/18.
 */
class TvShowEntitiy {

    var id: Int = 0
    var poster_path: String = ""
    var vote_average: Float = 0.toFloat()
    var vote_count: Int = 0
    var name: String = ""
    var overview: String = ""

}
