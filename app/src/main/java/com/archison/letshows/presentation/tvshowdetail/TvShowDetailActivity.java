package com.archison.letshows.presentation.tvshowdetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.archison.letshows.R;
import com.archison.letshows.presentation.InfiniteRecyclerViewScrollListener;
import com.archison.letshows.presentation.Navigator;
import com.archison.letshows.presentation.model.TvShowDetailModel;
import com.archison.letshows.presentation.model.TvShowModel;
import com.squareup.picasso.Picasso;

import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class TvShowDetailActivity extends AppCompatActivity implements TvShowDetailView {

    public static final String EXTRA_TV_SHOW_MODEL = "EXTRA_TV_SHOW_MODEL";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvshow_detail_imageview)
    ImageView tvShowImageView;
    @BindView(R.id.tvshow_title_textview)
    TextView tvShowTitleTextView;
    @BindView(R.id.tvshow_overview_textview)
    TextView tvShowOverviewTextView;
    @BindView(R.id.similar_tvshow_recyclerview)
    RecyclerView similarTvShowRecyclerView;
    @BindView(R.id.tvshow_detail_image_layout)
    View tvShowDetailLayout;
    @BindView(R.id.similar_shows_textview)
    TextView similarTvShowTitleTextView;
    @BindView(R.id.tvshow_vote_average_textview)
    TextView voteAverageTextView;
    @BindView(R.id.first_air_date_textview)
    TextView firstYearDateTextView;
    @BindView(R.id.status_textview)
    TextView statusTextView;
    @BindView(R.id.seasons_episodes_text_view)
    TextView seasonsEpisodesTextView;
    @BindView(R.id.popularity_textview)
    TextView popularityTextView;
    @BindView(R.id.genres_textview)
    TextView genresTextView;
    @BindView(R.id.tvshow_ratingbar)
    RatingBar ratingBar;

    private InfiniteRecyclerViewScrollListener mInfiniteRecyclerViewScrollListener;
    private TvShowSimilarListRecyclerAdapter   mAdapter;
    private TvShowDetailPresenter              mTvShowDetailPresenter;

    public static Intent getCallingIntent(Context context, TvShowModel tvShowModel) {
        Intent intent = new Intent(context, TvShowDetailActivity.class);
        intent.putExtra(EXTRA_TV_SHOW_MODEL, tvShowModel);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvshowdetail);
        ButterKnife.bind(this);
        initializeSupportActionBar();
        initializePresenter();
        initializeRecyclerView();
    }

    private void initializePresenter() {
        if (null != getIntent()
                && null != getIntent().getExtras()
                && getIntent().getExtras().containsKey(EXTRA_TV_SHOW_MODEL)) {
            setPresenter(new TvShowDetailPresenter(
                    this,
                    (TvShowModel) getIntent().getExtras().get(EXTRA_TV_SHOW_MODEL)));
        } else {
            Timber.e("Should have come with an EXTRA_TV_SHOW_MODEL extra!");
            finish();
        }
    }

    private void initializeSupportActionBar() {
        setSupportActionBar(mToolbar);
        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home_white_24dp);
        }
    }

    @Override
    public void populateBasicInfo(TvShowModel tvShowModel) {
        Picasso.with(this).load(tvShowModel.getCompleteImageUrlBig()).into(tvShowImageView);
        tvShowTitleTextView.setText(tvShowModel.getTitle());
        tvShowOverviewTextView.setText(tvShowModel.getOverview());
        voteAverageTextView.setText(tvShowModel.getFormattedVoteAverage());
        ratingBar.setRating(tvShowModel.getAverageRatingForFiveStars());
        setBackgroundColorFromImageDominantColor(tvShowModel);
    }

    private void populateTvShowDetailViews() {
        TvShowDetailModel tvShowDetailModel = mTvShowDetailPresenter.getTvShowDetailModel();
        int numSeasons = tvShowDetailModel.getNumberOfSeasons();
        int numEpisodes = tvShowDetailModel.getNumberOfEpisodes();
        seasonsEpisodesTextView.setText(getString(R.string.seasons_episodes_format,
                numSeasons,
                getResources().getQuantityString(R.plurals.seasons, numSeasons),
                numEpisodes,
                getResources().getQuantityString(R.plurals.episodes, numEpisodes)));
        firstYearDateTextView.setText(getString(R.string.year_format, tvShowDetailModel.getAirYearFormatted()));
        statusTextView.setText(tvShowDetailModel.getStatus());
        genresTextView.setText(tvShowDetailModel.getGenresFormatted());
        popularityTextView.setText(getString(R.string.popularity_format, String.valueOf(tvShowDetailModel.getPopularity())));
    }

    private void setBackgroundColorFromImageDominantColor(final TvShowModel tvShowModel) {
        tvShowDetailLayout.post(new Runnable() {
            @Override
            public void run() {
                try {
                    Bitmap image = BitmapFactory.decodeStream(
                            new URL(tvShowModel.getCompleteImageUrlSmall())
                                    .openConnection().getInputStream());
                    Palette.from(image).generate(new Palette.PaletteAsyncListener() {
                        public void onGenerated(Palette palette) {
                            tvShowDetailLayout.setBackgroundColor(
                                    palette.getDominantColor(Color.WHITE));
                        }
                    });
                } catch (Throwable t) {
                    Timber.e(t, "Throwable obtaining color for the detail background!");
                }
            }
        });

    }

    private void initializeRecyclerView() {
        mAdapter = new TvShowSimilarListRecyclerAdapter(
                mTvShowDetailPresenter.getSimilarTvShowModelList(), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = similarTvShowRecyclerView.getChildLayoutPosition(view);
                Navigator.navigateToTvShowDetail(
                        TvShowDetailActivity.this,
                        mTvShowDetailPresenter.getSimilarTvShowModelList().get(position),
                        view.findViewById(R.id.tvshow_imageview));
            }
        }, R.layout.similar_tv_show_item);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        similarTvShowRecyclerView.setLayoutManager(layoutManager);
        similarTvShowRecyclerView.setItemAnimator(new DefaultItemAnimator());
        similarTvShowRecyclerView.setAdapter(mAdapter);
        initializeRecyclerViewScrollListener(layoutManager);
        similarTvShowRecyclerView.addOnScrollListener(mInfiniteRecyclerViewScrollListener);
    }

    private void initializeRecyclerViewScrollListener(final LinearLayoutManager layoutManager) {
        mInfiniteRecyclerViewScrollListener =
                new InfiniteRecyclerViewScrollListener(layoutManager) {
            @Override
            protected void onLoadMore(int page, int totalItemsCount, RecyclerView recyclerView) {
                if (null != mTvShowDetailPresenter
                        && null != mTvShowDetailPresenter.getTvShowDetailModel()) {
                    mTvShowDetailPresenter.obtainSimilarTvShows(
                            page,
                            mTvShowDetailPresenter.getTvShowDetailModel().getShowId());
                }
            }
        };
    }

    public void setPresenter(TvShowDetailPresenter presenter) {
        this.mTvShowDetailPresenter = presenter;
    }

    @Override
    public void notifySimilarTvShowsLoaded() {
        similarTvShowRecyclerView.post(new Runnable() {
            public void run() {
                mAdapter.notifyDataSetChanged();
                similarTvShowRecyclerView.setVisibility(View.VISIBLE);
                similarTvShowTitleTextView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void notifyErrorLoadingSimilarTvShows() {
        similarTvShowTitleTextView.post(new Runnable() {
            @Override
            public void run() {
                similarTvShowRecyclerView.setVisibility(View.GONE);
                similarTvShowTitleTextView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void notifyTvShowDetailLoaded() {
        populateTvShowDetailViews();
    }

    @Override
    public void notifyErrorLoadingTvShowDetailInfo() {
        /* no-op */
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTvShowDetailPresenter.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getWindow().setSharedElementReturnTransition(null);
        getWindow().setSharedElementReenterTransition(null);
        tvShowImageView.setTransitionName(null);
    }
}
