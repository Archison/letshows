package com.archison.letshows.presentation.tvshowdetail;

import com.archison.letshows.data.service.TvShowServiceClient;
import com.archison.letshows.data.service.entity.PopularTvShowsEntity;
import com.archison.letshows.data.service.entity.TvShowDetailEntitiy;
import com.archison.letshows.data.service.entity.TvShowEntitiy;
import com.archison.letshows.presentation.model.ModelConverter;
import com.archison.letshows.presentation.model.TvShowDetailModel;
import com.archison.letshows.presentation.model.TvShowModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 *
 * Created by Luis on 17/2/18.
 */
class TvShowDetailPresenter {

    private static final int    STARTING_PAGE = 1;
    private static final String ON_ERROR      = "onError";
    private static final String ON_COMPLETE   = "onComplete";

    private final TvShowDetailView    mView;
    private final TvShowServiceClient mTvShowServiceClient;
    private final List<TvShowModel>   mSimilarTvShowModelList;
    private TvShowDetailModel         mTvShowDetailModel;
    private Disposable                mTvShowDetailDisposable;
    private Disposable                mSimilarTvShowsDisposable;

    TvShowDetailPresenter(TvShowDetailView view, TvShowModel tvShowModel) {
        mView = view;
        mSimilarTvShowModelList = new ArrayList<>();
        mTvShowServiceClient = new TvShowServiceClient();
        mView.populateBasicInfo(tvShowModel);
        obtainTvShowDetailInfo(tvShowModel.getShowId());
        obtainSimilarTvShows(STARTING_PAGE, tvShowModel.getShowId());
    }

    private void obtainTvShowDetailInfo(int tvShowId) {
        mTvShowServiceClient.getTvShowDetail(tvShowId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<TvShowDetailEntitiy>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mTvShowDetailDisposable = d;
                    }

                    @Override
                    public void onNext(TvShowDetailEntitiy tvShowDetailEntitiy) {
                        if (null != tvShowDetailEntitiy) {
                            mTvShowDetailModel = ModelConverter.convertToTvShowDetailModel(tvShowDetailEntitiy);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, ON_ERROR);
                        mView.notifyErrorLoadingTvShowDetailInfo();
                    }

                    @Override
                    public void onComplete() {
                        Timber.i(ON_COMPLETE);
                        mView.notifyTvShowDetailLoaded();
                    }
                });
    }

    void obtainSimilarTvShows(int page, int tvShowId) {
        mTvShowServiceClient.getSimilarTvShows(tvShowId, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PopularTvShowsEntity>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mSimilarTvShowsDisposable = d;
                    }

                    @Override
                    public void onNext(PopularTvShowsEntity popularTvShowsResponse) {
                        if (null != popularTvShowsResponse && null != popularTvShowsResponse.getResults()) {
                            for (TvShowEntitiy tvShowEntitiy : popularTvShowsResponse.getResults()) {
                                mSimilarTvShowModelList.add(ModelConverter.convertToTvShowModel(tvShowEntitiy));
                            }
                            mView.notifySimilarTvShowsLoaded();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, ON_ERROR);
                        mView.notifyErrorLoadingSimilarTvShows();
                    }

                    @Override
                    public void onComplete() {
                        Timber.i(ON_COMPLETE);
                    }
                });
    }

    TvShowDetailModel getTvShowDetailModel() {
        return mTvShowDetailModel;
    }

    List<TvShowModel> getSimilarTvShowModelList() {
        return mSimilarTvShowModelList;
    }

    void onStop() {
        if (null != mSimilarTvShowsDisposable && !mSimilarTvShowsDisposable.isDisposed()) {
            mSimilarTvShowsDisposable.dispose();
        }

        if (null != mTvShowDetailDisposable && !mTvShowDetailDisposable.isDisposed()) {
            mTvShowDetailDisposable.dispose();
        }
    }
}
