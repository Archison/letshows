package com.archison.letshows.presentation.tvshowslist;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 *
 * Created by Luis on 17/2/18.
 */
public class GridSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int separation;

    GridSpaceItemDecoration(int spanCount, int separation) {
        this.spanCount = spanCount;
        this.separation = separation;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        int column = position % spanCount;

        outRect.left = separation - column * separation / spanCount;
        outRect.right = (column + 1) * separation / spanCount;
        if (position < spanCount) {
            outRect.top = separation;
        }
    }
}
