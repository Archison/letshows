package com.archison.letshows.presentation.tvshowslist;

import android.app.Activity;
import android.view.View;

import com.archison.letshows.data.service.TvShowServiceClient;
import com.archison.letshows.data.service.entity.PopularTvShowsEntity;
import com.archison.letshows.data.service.entity.TvShowEntitiy;
import com.archison.letshows.presentation.Navigator;
import com.archison.letshows.presentation.model.ModelConverter;
import com.archison.letshows.presentation.model.TvShowModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 *
 * Created by Luis on 16/2/18.
 */
class TvShowsListPresenter {

    private static final int STARTING_PAGE = 1;

    private final TvShowsListView mView;
    private List<TvShowModel>     mTvShowList;
    private TvShowServiceClient   mTvShowServiceClient;
    private Disposable            mTvShowDisposable;

    TvShowsListPresenter(TvShowsListView tvShowsListView) {
        mView = tvShowsListView;
        mTvShowList = new ArrayList<>();
        mTvShowServiceClient = new TvShowServiceClient();
        loadTvShowsFromApi(STARTING_PAGE);
    }

    void loadTvShowsFromApi(int page) {
        mTvShowServiceClient.getPopularTvShows(page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PopularTvShowsEntity>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        mTvShowDisposable = d;
                    }

                    @Override
                    public void onNext(PopularTvShowsEntity popularTvShowsResponse) {
                        if (null != popularTvShowsResponse && null != popularTvShowsResponse.getResults()) {
                            for (TvShowEntitiy tvShowEntitiy : popularTvShowsResponse.getResults()) {
                                mTvShowList.add(ModelConverter.convertToTvShowModel(tvShowEntitiy));
                            }
                            mView.notifyDataLoadedFromApi();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "onError");
                    }

                    @Override
                    public void onComplete() {
                        Timber.i("onComplete");
                    }
                });
    }

    void navigateToTvShowDetail(Activity activity, int position, View view) {
        Navigator.navigateToTvShowDetail(activity, mTvShowList.get(position), view);
    }

    List<TvShowModel> getTvShowList() {
        return mTvShowList;
    }

    void onStop() {
        if (null != mTvShowDisposable && !mTvShowDisposable.isDisposed()) {
            mTvShowDisposable.dispose();
        }
    }

}
