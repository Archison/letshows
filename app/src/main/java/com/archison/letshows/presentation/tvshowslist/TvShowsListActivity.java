package com.archison.letshows.presentation.tvshowslist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.archison.letshows.R;
import com.archison.letshows.presentation.InfiniteRecyclerViewScrollListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Created by Luis on 16/2/18.
 */
public class TvShowsListActivity extends AppCompatActivity implements TvShowsListView {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tvshow_list_recyclerview)
    RecyclerView tvShowListRecyclerView;

    TvShowNormalListRecyclerAdapter    mAdapter;
    InfiniteRecyclerViewScrollListener mInfiniteRecyclerViewScrollListener;
    TvShowsListPresenter               mPresenter;

    private View.OnClickListener mOnTvShowClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mPresenter.navigateToTvShowDetail(
                    TvShowsListActivity.this,
                    tvShowListRecyclerView.getChildLayoutPosition(view),
                    view
            );
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvshowslist);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mPresenter = new TvShowsListPresenter(this);
        initializeRecyclerView();
        setToolbarClickListener();
    }

    private void setToolbarClickListener() {
        mToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GridLayoutManager gridLayoutManager =
                        (GridLayoutManager) tvShowListRecyclerView.getLayoutManager();
                gridLayoutManager.scrollToPosition(0);
            }
        });
    }

    private void initializeRecyclerView() {
        mAdapter = new TvShowNormalListRecyclerAdapter(
                mPresenter.getTvShowList(), mOnTvShowClickListener, R.layout.normal_tv_show_item);
            GridLayoutManager layoutManager =
                    new GridLayoutManager(getApplicationContext(), 2);
        tvShowListRecyclerView.setLayoutManager(layoutManager);
        tvShowListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        tvShowListRecyclerView.addItemDecoration(
                new GridSpaceItemDecoration(2, 50));
        tvShowListRecyclerView.setAdapter(mAdapter);
        initializeRecyclerViewScrollListener(layoutManager);
        tvShowListRecyclerView.addOnScrollListener(mInfiniteRecyclerViewScrollListener);
    }

    private void initializeRecyclerViewScrollListener(final GridLayoutManager layoutManager) {
        mInfiniteRecyclerViewScrollListener = new InfiniteRecyclerViewScrollListener(layoutManager) {
            @Override
            protected void onLoadMore(int page, int totalItemsCount, RecyclerView recyclerView) {
                mPresenter.loadTvShowsFromApi(page);
            }
        };
    }

    @Override
    public void notifyDataLoadedFromApi() {
        tvShowListRecyclerView.post(new Runnable() {
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.onStop();
    }

    public void setPresenter(TvShowsListPresenter presenter) {
        mPresenter = presenter;
    }
}
