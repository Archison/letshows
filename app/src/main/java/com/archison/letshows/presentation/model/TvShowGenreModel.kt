package com.archison.letshows.presentation.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *
 * Created by Luis on 18/2/18.
 */
@SuppressLint("ParcelCreator")
@Parcelize
class TvShowGenreModel (

    var id: Int? = 0,
    var name: String? = null

): Parcelable