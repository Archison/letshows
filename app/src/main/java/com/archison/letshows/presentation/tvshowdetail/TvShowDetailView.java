package com.archison.letshows.presentation.tvshowdetail;

import com.archison.letshows.presentation.model.TvShowModel;

/**
 *
 * Created by Luis on 17/2/18.
 */
interface TvShowDetailView {

    void notifySimilarTvShowsLoaded();

    void notifyErrorLoadingSimilarTvShows();

    void notifyTvShowDetailLoaded();

    void notifyErrorLoadingTvShowDetailInfo();

    void populateBasicInfo(TvShowModel tvShowModel);

}
