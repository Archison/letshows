package com.archison.letshows.presentation.model

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *
 * Created by Luis on 17/2/18.
 */
@SuppressLint("ParcelCreator")
@Parcelize
class TvShowDetailModel (

        val showId: Int = 0,
        val imageUrl: String = "",
        val title: String = "",
        val overview: String = "",
        val averageRating: Float = 0.toFloat(),
        val numberOfRating: Int = 0,
        val genres: List<TvShowGenreModel> = arrayListOf(),
        val numberOfEpisodes: Int = 0,
        val numberOfSeasons: Int = 0,
        val firstAirDate: String = "",
        val status: String = "",
        val popularity: Int = 0

): Parcelable {

    // Created this method without considering Locale, etc. as this first version will be US based.
    fun getAirYearFormatted(): String? {
        if (!firstAirDate.isEmpty()) {
            return firstAirDate.split("-")[0]
        }
        return firstAirDate
    }

    fun getGenresFormatted(): String? {
        val genresStringBuilder = StringBuilder("")
        if (genres.isNotEmpty()) {
            for (genre in genres) {
                if(genresStringBuilder.isNotEmpty()) {
                    genresStringBuilder.append(", ")
                }
                genresStringBuilder.append(genre.name)
            }
        }
        return genresStringBuilder.toString()
    }

}