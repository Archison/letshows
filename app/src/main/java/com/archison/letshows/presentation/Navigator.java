package com.archison.letshows.presentation;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.view.View;

import com.archison.letshows.R;
import com.archison.letshows.presentation.model.TvShowModel;
import com.archison.letshows.presentation.tvshowdetail.TvShowDetailActivity;

/**
 *
 * Created by Luis on 18/2/18.
 */
public class Navigator {

    public static void navigateToTvShowDetail(Activity activity, TvShowModel tvShowModel, View sharedView) {
        Intent intent = TvShowDetailActivity.getCallingIntent(activity, tvShowModel);
        String transitionName = activity.getString(R.string.tvshow_image);
        ActivityOptions transitionOptions = ActivityOptions.makeSceneTransitionAnimation(activity, sharedView, transitionName);
        activity.startActivity(intent, transitionOptions.toBundle());
    }

}
