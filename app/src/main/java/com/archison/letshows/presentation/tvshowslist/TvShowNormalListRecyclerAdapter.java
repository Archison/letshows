package com.archison.letshows.presentation.tvshowslist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.archison.letshows.R;
import com.archison.letshows.presentation.model.TvShowModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 *
 * Created by Luis on 16/2/18.
 */
public class TvShowNormalListRecyclerAdapter extends RecyclerView.Adapter<TvShowNormalListRecyclerAdapter.ViewHolder> {

    private View.OnClickListener mOnClickListener;
    private List<TvShowModel>    mMovieModelList;
    private int                  mItemLayoutId;

    TvShowNormalListRecyclerAdapter(List<TvShowModel> movieModelList,
                                    View.OnClickListener onClickListener,
                                    int itemLayoutId) {
        mMovieModelList = movieModelList;
        mOnClickListener = onClickListener;
        mItemLayoutId = itemLayoutId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(mItemLayoutId, parent, false);
        view.setOnClickListener(mOnClickListener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        TvShowModel tvShowModel = mMovieModelList.get(position);
        Context context = holder.pictureImageView.getContext();
        Picasso.with(context).load(tvShowModel.getCompleteImageUrlSmall()).into(holder.pictureImageView);
        Picasso.with(context).load(tvShowModel.getCompleteImageUrlBig()).fetch();
        holder.simpleTextView.setText(tvShowModel.getTitle());
        holder.voteAverageTextView.setText(tvShowModel.getFormattedVoteAverage());
        holder.ratingBar.setRating(tvShowModel.getAverageRatingForFiveStars());
    }

    @Override
    public int getItemCount() {
        return mMovieModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pictureImageView;
        TextView simpleTextView;
        TextView voteAverageTextView;
        RatingBar ratingBar;

        ViewHolder(View view) {
            super(view);
            pictureImageView = view.findViewById(R.id.tvshow_imageview);
            simpleTextView = view.findViewById(R.id.tvshow_item_textview);
            voteAverageTextView = view.findViewById(R.id.tvshow_vote_average_textview);
            ratingBar = view.findViewById(R.id.tvshow_ratingbar);
        }
    }
}
