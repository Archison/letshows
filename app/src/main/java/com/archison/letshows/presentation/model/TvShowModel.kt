package com.archison.letshows.presentation.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.archison.letshows.BuildConfig
import kotlinx.android.parcel.Parcelize

/**
 *
 * Created by Luis on 17/2/18.
 */
@SuppressLint("ParcelCreator")
@Parcelize
class TvShowModel internal constructor (
        val showId: Int,
        val imageUrl: String?,
        val title: String,
        val overview: String?,
        val averageRating: Float,
        val numberOfRating: Int?
) : Parcelable {

    override fun toString(): String {
        return "TvShowModel(showId=$showId, imageUrl='$imageUrl', title='$title', overview='$overview', averageRating=$averageRating, numberOfRating=$numberOfRating)"
    }

    fun getAverageRatingForFiveStars(): Float {
        return averageRating / 2
    }

    fun getFormattedVoteAverage(): String {
        return String.format("%s (%s)", averageRating.toString(), numberOfRating.toString())
    }

    fun getCompleteImageUrlSmall(): String {
        return BuildConfig.TV_SHOW_IMAGE_BASE_URL + "w500" + this.imageUrl
    }

    fun getCompleteImageUrlBig(): String {
        return BuildConfig.TV_SHOW_IMAGE_BASE_URL + "w780" + this.imageUrl
    }
}
