package com.archison.letshows.presentation.model;

import android.support.annotation.NonNull;

import com.archison.letshows.data.service.entity.TvShowDetailEntitiy;
import com.archison.letshows.data.service.entity.TvShowEntitiy;
import com.archison.letshows.data.service.entity.TvShowGenreEntitiy;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Luis on 17/2/18.
 */
public class ModelConverter {

    public static TvShowModel convertToTvShowModel(TvShowEntitiy tvShowEntitiy) {
        Integer showId = tvShowEntitiy.getId();
        String imageUrl = tvShowEntitiy.getPoster_path();
        String title = tvShowEntitiy.getName();
        String overview = tvShowEntitiy.getOverview();
        float averageRating = tvShowEntitiy.getVote_average();
        int numberOfRating = tvShowEntitiy.getVote_count();
        return new TvShowModel(showId, imageUrl, title, overview, averageRating, numberOfRating);
    }

    public static TvShowDetailModel convertToTvShowDetailModel(TvShowDetailEntitiy tvShowDetailEntitiy) {
        Integer showId = tvShowDetailEntitiy.getId();
        String imageUrl = tvShowDetailEntitiy.getPoster_path();
        String title = tvShowDetailEntitiy.getName();
        String overview = tvShowDetailEntitiy.getOverview();
        float averageRating = tvShowDetailEntitiy.getVote_average();
        int numberOfRating = tvShowDetailEntitiy.getVote_count();
        List<TvShowGenreModel> genreModelList = obtainGenreList(tvShowDetailEntitiy);
        String firstAirDate = tvShowDetailEntitiy.getFirst_air_date();
        String status = tvShowDetailEntitiy.getStatus();
        int popularity = (int) tvShowDetailEntitiy.getPopularity();

        return new TvShowDetailModel(
                showId, imageUrl, title, overview, averageRating, numberOfRating,
                genreModelList, tvShowDetailEntitiy.getNumber_of_episodes(),
                tvShowDetailEntitiy.getNumber_of_seasons(), firstAirDate, status, popularity);
    }

    @NonNull
    private static List<TvShowGenreModel> obtainGenreList(TvShowDetailEntitiy tvShowDetailEntitiy) {
        List<TvShowGenreModel> genreModelList = new ArrayList<>();
        for (TvShowGenreEntitiy tvShowGenreEntity : tvShowDetailEntitiy.getGenres()){
            genreModelList.add(
                    new TvShowGenreModel(tvShowGenreEntity.getId(), tvShowGenreEntity.getName()));
        }
        return genreModelList;
    }

}
