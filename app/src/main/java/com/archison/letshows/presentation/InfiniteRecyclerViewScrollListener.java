package com.archison.letshows.presentation;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Based in an example seen in https://github.com/codepath/android_guides as I've never implemented
 * an infinite paginated scroll list.
 *
 * Created by Luis on 16/2/18.
 */
public abstract class InfiniteRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private final LinearLayoutManager mLayoutManager;

    private boolean loading                 = true;
    private int     visibleThreshold        = 5;
    private int     currentPage             = 1;
    private int     startingPageIndex       = 1;
    private int     previousTotalItemCount  = 0;

    protected InfiniteRecyclerViewScrollListener(GridLayoutManager layoutManager) {
        this.mLayoutManager = layoutManager;
        visibleThreshold = visibleThreshold * layoutManager.getSpanCount();
    }

    protected InfiniteRecyclerViewScrollListener(LinearLayoutManager layoutManager) {
        this.mLayoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        int lastVisibleItemPosition;
        int totalItemCount = mLayoutManager.getItemCount();

        lastVisibleItemPosition = mLayoutManager.findLastVisibleItemPosition();

        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }

        if (loading && (totalItemCount > previousTotalItemCount)){
            loading = false;
            previousTotalItemCount = totalItemCount;
        }

        if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
            currentPage ++;
            onLoadMore(currentPage, totalItemCount, recyclerView);
            loading = true;
        }
    }

    protected abstract void onLoadMore(int page, int totalItemsCount, RecyclerView recyclerView);

}
