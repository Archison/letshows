package com.archison.letshows;

import android.app.Application;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

/**
 *
 * Created by Luis on 16/2/18.
 */
public class LetShowsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initializeTimber();
    }

    private void initializeTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new ProdTree());
        }
    }

    private class ProdTree extends Timber.Tree {
        @Override
        protected void log(int priority, @Nullable String tag, @NotNull String message, @Nullable Throwable t) {
            // TODO: We could send error logs to Crashlytics here, for example
        }
    }
}
