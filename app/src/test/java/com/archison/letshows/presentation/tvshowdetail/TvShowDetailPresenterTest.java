package com.archison.letshows.presentation.tvshowdetail;

import com.archison.letshows.presentation.model.TvShowModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;

/**
 * TODO: I'd have to use Dagger 2 or another DI framework to make it easier to test the presenter.
 *
 * Created by Luis on 23/2/18.
 */
@RunWith(JUnit4.class)
public class TvShowDetailPresenterTest {

    private static final String TITLE = "Interstellar";

    @Mock
    private TvShowDetailView mTvShowDetailView;
    private TvShowModel mTvShowModel;
    private TvShowDetailPresenter mTvShowDetailPresenter;

    @Before
    public void setUp() throws Exception {
        // initMockTvShowModel();
        // mTvShowDetailPresenter = new TvShowDetailPresenter(mTvShowDetailView, mTvShowModel);
    }

    @Test
    public void toDo() throws Exception {
        /**/
    }

    private void initMockTvShowModel() {
        mTvShowModel = new TvShowModel(
                1,
                "http://image.tmdb.org/t/p/w500/nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg",
                TITLE,
                "An amazing movie.",
                10.0f,
                100000
        );
    }

}