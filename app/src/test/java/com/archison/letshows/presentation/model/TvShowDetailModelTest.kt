package com.archison.letshows.presentation.model

import org.junit.Test

/**
 *
 * Created by Luis on 23/2/18.
 */
class TvShowDetailModelTest {

    val showId = 1
    val imageUrl = "nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg"
    val title = "Interestellar"
    val overview = "An amazing movie."
    val averageRating = 10.toFloat()
    val numberOfRating = 100000
    val genres = listOf(TvShowGenreModel(1, "Awesome"), TvShowGenreModel(2, "Sci-Fi"))
    val numberOfEpisodes = 1
    val numberOfSeasons = 1
    val firstAirDate = "2014-10-26"
    val status = "Ended"
    val popularity = 100

    var tvShowDetailModel: TvShowDetailModel =
            TvShowDetailModel(
                    showId,
                    imageUrl,
                    title,
                    overview,
                    averageRating,
                    numberOfRating,
                    genres,
                    numberOfEpisodes,
                    numberOfSeasons,
                    firstAirDate,
                    status,
                    popularity
            )


    @Test
    fun getAirYearFormatted() {
        assert(tvShowDetailModel.getAirYearFormatted() == "2014")
    }

    @Test
    fun getGenresFormatted() {
        assert(tvShowDetailModel.getGenresFormatted() == "Awesome, Sci-Fi")
    }

    @Test
    fun getShowId() {
        assert(tvShowDetailModel.showId == showId)
    }

    @Test
    fun getImageUrl() {
        assert(tvShowDetailModel.imageUrl == imageUrl)
    }

    @Test
    fun getTitle() {
        assert(tvShowDetailModel.title == title)
    }

    @Test
    fun getOverview() {
        assert(tvShowDetailModel.overview == overview)
    }

    @Test
    fun getAverageRating() {
        assert(tvShowDetailModel.averageRating == averageRating)
    }

    @Test
    fun getNumberOfRating() {
        assert(tvShowDetailModel.numberOfRating == numberOfRating)
    }

    @Test
    fun getGenres() {
        assert(tvShowDetailModel.genres == genres)
    }

    @Test
    fun getNumberOfEpisodes() {
        assert(tvShowDetailModel.numberOfEpisodes == numberOfEpisodes)
    }

    @Test
    fun getNumberOfSeasons() {
        assert(tvShowDetailModel.numberOfSeasons == numberOfSeasons)
    }

    @Test
    fun getFirstAirDate() {
        assert(tvShowDetailModel.firstAirDate == firstAirDate)
    }

    @Test
    fun getStatus() {
        assert(tvShowDetailModel.status == status)
    }

    @Test
    fun getPopularity() {
        assert(tvShowDetailModel.popularity == popularity)
    }

}