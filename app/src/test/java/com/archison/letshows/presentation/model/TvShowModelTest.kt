package com.archison.letshows.presentation.model

import com.archison.letshows.BuildConfig
import org.junit.Test

/**
 *
 * Created by Luis on 23/2/18.
 */
class TvShowModelTest {

    val showId = 1
    val imageUrl = "nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg"
    val title = "Interestellar"
    val overview = "An amazing movie."
    val averageRating = 10.toFloat()
    val numberOfRating = 100000

    var tvShowModel: TvShowModel =
            TvShowModel(
                    showId,
                    imageUrl,
                    title,
                    overview,
                    averageRating,
                    numberOfRating
            )

    @Test
    fun getShowId() {
        assert(tvShowModel.showId == showId)
    }

    @Test
    fun getImageUrl() {
        assert(tvShowModel.imageUrl == imageUrl)
    }

    @Test
    fun getTitle() {
        assert(tvShowModel.title == title)
    }

    @Test
    fun getOverview() {
        assert(tvShowModel.overview == overview)
    }

    @Test
    fun getAverageRating() {
        assert(tvShowModel.averageRating == averageRating)
    }

    @Test
    fun getNumberOfRating() {
        assert(tvShowModel.numberOfRating == numberOfRating)
    }

    @Test
    fun getAverageRatingForFiveStars() {
        assert(tvShowModel.getAverageRatingForFiveStars() == (averageRating / 2))
    }

    @Test
    fun getFormattedVoteAverage() {
        assert(tvShowModel.getFormattedVoteAverage() ==
                (averageRating.toString() + "(" + numberOfRating.toString() + ")"))
    }

    @Test
    fun getCompleteImageUriSmall() {
        assert(tvShowModel.getCompleteImageUrlSmall() == BuildConfig.TV_SHOW_IMAGE_BASE_URL + "w500" + imageUrl)
    }

    @Test
    fun getCompleteImageUriBig() {
        assert(tvShowModel.getCompleteImageUrlSmall() == BuildConfig.TV_SHOW_IMAGE_BASE_URL + "w780" + imageUrl)
    }

}